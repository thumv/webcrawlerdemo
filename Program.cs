﻿using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace CrawlerCSharp
{
    class Program
    {
        private static List<Link> siteURLS;
        private static HttpClient httpClient;
        private static string rootURL;
        private static bool newURLFound;
       static void Main(string[] args)
        {
            httpClient = new HttpClient();

            siteURLS = new List<Link>();
            //rootURL = "http://valentin-thum.de/";
            rootURL = Console.ReadLine();
            siteURLS.Add(new Link
            {
                URL = rootURL
            });
            newURLFound = false;
            CrawlSite();
        }

        private static void CrawlSite()
        {
            do
            {
                if (newURLFound == false)
                    StartCrawlerAsync(rootURL).Wait();

                else {
                    newURLFound = false;
                    for (int i = 0; i < siteURLS.Count; i++)
                    {
                        StartCrawlerAsync(siteURLS[i].URL).Wait();
                    }
                }
            } while (newURLFound == true);
        }

        private static async Task StartCrawlerAsync(string url)
        {
            var html = await httpClient.GetStringAsync(url);       
            var htmlDocument = new HtmlDocument();
            htmlDocument.LoadHtml(html);
            var links = htmlDocument.DocumentNode.Descendants("a").Where(node => node.GetAttributeValue("href", "").Any()).ToList();

            foreach (var a in links)
            {
                
                var link = new Link
                {
                    URL = a.ChildAttributes("href").FirstOrDefault().Value
                };

                if (!link.URL.StartsWith(url))
                    continue;

                if (!link.URL.EndsWith("/"))
                link.URL += "/";

                if (link.URL.Contains("#")) //|| link.URL.Equals(url) //in case root directory is not to be included
                    continue;


                bool isDouble = false;
                for (int i = 0; i < siteURLS.Count; i++)
                {
                    if (siteURLS[i].URL.Equals(link.URL))
                        isDouble = true;
                }
                if (!isDouble) {
                     siteURLS.Add(link);
                    newURLFound = true;
                    Console.WriteLine(link.URL);
                }
            }


            var dir = Directory.GetCurrentDirectory();
            Directory.CreateDirectory(dir + "\\crawledSite\\");
            for (int i = 0; i < siteURLS.Count; i++)
            {
                string fileName = URLToFilename(siteURLS[i].URL) + ".html";
                using (FileStream fileStream = new FileStream(dir + "\\crawledSite\\" + fileName, FileMode.Create))
                {
                    htmlDocument.Save(fileStream);
                }
            } 
        }

        private static String URLToFilename(string url)
        {
            if(!url.Equals(rootURL))
                url = url.Replace(rootURL,"");
            String newString = url.Replace("http", "").Replace(":", "").Replace("/","");
       
            return newString;
        }

    }
}
